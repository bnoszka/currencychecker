import { Component, OnInit } from '@angular/core';

import {NgbSlideEvent} from '@ng-bootstrap/ng-bootstrap';
import {CurrencyService} from './currency.service';

@Component({
  selector: 'app-currency',
  templateUrl: './currency.component.html',
  styleUrls: ['./currency.component.sass'],
  providers: [CurrencyService]
})
export class CurrencyComponent implements OnInit {

  constructor(private currencyService: CurrencyService) {}

  ngOnInit() {
    this.currencyService.init();
  }

  onSlide(slideEvent: NgbSlideEvent) {
    this.currencyService.refresh( slideEvent.current.charAt(slideEvent.current.length - 1));
  }

}
