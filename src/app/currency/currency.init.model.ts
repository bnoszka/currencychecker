
export interface InitCurrency {
  image: string;
  url: string;
  rate: number;
  from: string;
  to: string;
}
