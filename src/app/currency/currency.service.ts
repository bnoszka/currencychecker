import {HttpClient} from '@angular/common/http';
import {InitCurrency} from './currency.init.model';
import {Injectable} from '@angular/core';


const initCurrencies = [
  {
    url: 'https://api.exchangeratesapi.io/latest?base=CHF&symbols=USD',
    image: 'https://source.unsplash.com/featured/800x300?Switzerland',
    rate: null,
    from: 'CHF',
    to: 'USD',
  },
  {
    url: 'https://api.exchangeratesapi.io/latest?base=GBP&symbols=EUR',
    image: 'https://source.unsplash.com/featured/800x300?Europe',
    rate: null,
    from: 'GBP',
    to: 'EUR'
  },
  {
    url: 'https://api.exchangeratesapi.io/latest?base=USD&symbols=GBP',
    image: 'https://source.unsplash.com/featured/800x300?Usa',
    rate: null,
    from: 'USD',
    to: 'GBP'
  }
];

@Injectable()
export class CurrencyService {

  initCurrencies: InitCurrency[];

  constructor(protected http: HttpClient) {
    this.initCurrencies = initCurrencies;

  }

  init() {
    this.initCurrencies.forEach(initCurrency => {
      this.http.get(initCurrency.url + '&' + Math.random() ).subscribe((value: any) => {
        initCurrency.rate = value.rates[initCurrency.to];
      });
    });
  }

  refresh(current): void {
    this.initCurrencies[current].rate = null;
    this.http.get(this.initCurrencies[current].url + '&' + Math.random()).subscribe((value: any) => {
      this.initCurrencies[current].rate = value.rates[this.initCurrencies[current].to];
    });
  }
}
