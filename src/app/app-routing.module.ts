import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CurrencyComponent} from './currency/currency.component';


const routes: Routes = [
  {
    path: 'overview',
    component: CurrencyComponent,
    data: { title: 'Currency Overview' }
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
